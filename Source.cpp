﻿// C++
#include <iostream>
#include <map>
#include <cstdio>
#include <ctime>
#include <map>
#include <time.h>
#include <thread>         // std::thread
#include <mutex>          // std::mutex
#include <fstream>
#include <string>
// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/video/tracking.hpp>
// PnP Problem
#include "Mesh.h"
#include "Model.h"
#include "PnPProblem.h"
#include "RobustMatcher.h"
#include "ModelRegistration.h"
#include "Utils.h"

//3D models
#include "House.h"

using namespace cv;
using namespace std;

#define NUMCORNERS 8
#define HE 3 //define the length of half an edge
#define SHOWTHRESH true
#define SHOWBLOB false
#define EVA false
#define TESTMODE false
#define SHOWHOUSE true
#define SHOWCROSS false
#define WRITEFRAME false
#define MINCOUNT 20
#define MAXCOUNT 10000
#define CAMNUM 1
#define LC 1



#define FRAMEWIDTH 640
#define FRAMEHEIGHT 480


/**  GLOBAL VARIABLES  **/

//for evalutaion

double gtTheta = 0;
double gtPhi = 20.5;
bool breakWhileFlag = false;

int evaCount = 0;

/**  blob experiments**/
Mat frameBlob(Mat(FRAMEHEIGHT, FRAMEWIDTH, CV_8U));

//corners and their valid map
KeyPoint corners[NUMCORNERS];
map<int, bool> isValidCorner;



// Kalman Filter parameters
int minInliersKalman = 3;    // Kalman threshold updating
int minPnPRANSAC = 0;

// PnP parameters
int pnpMethod = SOLVEPNP_P3P;

Matx33d _K = Matx33d(584.36435949705105, 0.0, 319.5,
	0, 584.36435949705105, 239.5,
	0, 0, 1);

// RANSAC parameters
int iterationsCount = 100;      // number of Ransac iterations.
float reprojectionError = 2.0;  // maximum allowed distance to consider it an inlier.
double confidence = 0.95;        // ransac successful confidence.

// Some basic colors
Scalar red(0, 0, 255);
Scalar green(0, 255, 0);
Scalar blue(255, 0, 0);
Scalar yellow(0, 255, 255);



//Multithreading
std::mutex mtx;           // mutex for critical section


/**	END of the global variables**/

struct lightingParam{
	int iLowH;
	int iHighH;
	int iLowS;
	int iHighS;
	int iLowV;
	int iHighV;
};


/**Function Headers**/
SimpleBlobDetector::Params initBlobParas();
void nameWindows();
void createHSVControlBar(lightingParam &lp);
void createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio);
void detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index);

void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt);
void updateKalmanFilter(KalmanFilter &KF, Mat &measurements,
	Mat &translation_estimated, Mat &rotation_estimated);
void fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured);

void drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner);
void initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners);

void initiPointModel(vector<Point3f> &list_points3d_model);
void detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index);
void initMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);
void readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars);
void writeParams(vector<lightingParam> lParams);
/**END of the function headers**/

// for drawing contours of the user selection
vector<Point> cornerPts;
bool isSelectionFinished = false;
const float PI = 3.14;
bool isMannual = false;
map<int, string> vertexColors;
bool isAutoAdjusting = false;
vector<lightingParam> lParams;


void CallBackFunc(int event, int x, int y, int flags, void* userdata)					//mouse callback functions http://opencv-srf.blogspot.com/2011/11/mouse-events.html
{
	if (flags == (EVENT_FLAG_CTRLKEY + EVENT_FLAG_LBUTTON)){
		//confirmation of finishing the task
		cout << "Selection Finished" << endl;
		isSelectionFinished = true;
	}
	else if (event == EVENT_LBUTTONDOWN)
	{
		cout << "Left button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		cornerPts.push_back(Point(x, y));
	}
	else if (event == EVENT_RBUTTONDOWN)
	{
		cout << "Right button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		cornerPts.pop_back();
	}
	
	else if (event == EVENT_MBUTTONDOWN)
	{
		//cout << "Middle button of the mouse is clicked - position (" << x << ", " << y << ")" << endl;
		//do nothing
	}
	else if (event == EVENT_MOUSEMOVE)
	{
		//cout << "Mouse move over the window - position (" << x << ", " << y << ")" << endl;
		//do nothing
	}
	
}

Mat cropImage(Mat img, Point2f center, float radius,bool inverted){
	Mat1b mask(img.size(), uchar(0));
	circle(mask, center, radius, Scalar(255), CV_FILLED);
	if (inverted)
	{

		bitwise_not(mask, mask);
	}
	// Create a black image
	Mat res = Mat::zeros(img.size(), img.type());
	img.copyTo(res, mask);
	//imshow("Result", res);
	return res/255;
}

double computeArea(Mat croppedImg){

	Moments mu = moments(croppedImg, false);
	double res = mu.m00;
	return res;
}

void updateLP(lightingParam lp){
	setTrackbarPos("LowH", "Control", lp.iLowH);
	setTrackbarPos("HighH", "Control", lp.iHighH);
	setTrackbarPos("LowS", "Control", lp.iLowS);
	setTrackbarPos("HighS", "Control", lp.iHighS);
	setTrackbarPos("LowV", "Control", lp.iLowV);
	setTrackbarPos("HighV", "Control", lp.iHighV);
}

double ptDist(Point2f p1, Point2f p2){
	Point diff = p1 - p2;
	//cout << "dist is :" << cv::sqrt(diff.x*diff.x + diff.y*diff.y) << endl;
	return cv::sqrt(diff.x*diff.x + diff.y*diff.y);
}

void selectCorner(Point2f &center, float &radius, int cornerIndex, VideoCapture cap){
	Mat frame, frame_HSV, frame_vis, frame_selection, frame_thresholded;

	//set the callback function for any mouse event
	setMouseCallback("RGB view", CallBackFunc, NULL);

	//initialize variables
	cornerPts.clear();
	isSelectionFinished = false;
	lightingParam lp;

	int k = -1;
	//the selection task
	while ((k = waitKey(30)) != 27 && !isSelectionFinished)
	{
		cap >> frame;
		frame_vis = frame.clone();
		cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);
		frame_selection = cv::Mat::zeros(FRAMEHEIGHT, FRAMEWIDTH, frame.type());

		string text = string("Please select the interior of corner: ") + vertexColors[cornerIndex];
		drawText(frame_vis, text, Scalar(255));
		//drawText(frame_vis, text, Scalar(255, 255, 255));

		if (cornerIndex>0)
		{
			lp = lParams[cornerIndex - 1];
			inRange(frame_HSV, Scalar(lp.iLowH, lp.iLowS, lp.iLowV), Scalar(lp.iHighH, lp.iHighS, lp.iHighV), frame_thresholded); //Threshold the image
		}

		//draw the contour of the corner
		if (cornerPts.size()>0)
		{
			for (int i = 0; i < cornerPts.size(); i++)
			{
				cv::circle(frame_vis, cornerPts[i], 1, Scalar(255, 0, 0), 2);
				cv::circle(frame_selection, cornerPts[i], 1, Scalar(255, 0, 0), 2);
			}
			if (cornerPts.size()>1)
			{
				for (int i = 0; i < cornerPts.size() - 1; i++)
				{
					cv::line(frame_vis, cornerPts[i], cornerPts[i + 1], Scalar(0, 255, 0));
					cv::line(frame_selection, cornerPts[i], cornerPts[i + 1], Scalar(0, 255, 0));
				}
			}

		}

		if (cornerIndex > 0)
		{
			GaussianBlur(frame_thresholded, frame_thresholded, Size(9, 9), 0, 0);
			cv::imshow("Thresholded Image", frame_thresholded);
		}

		cv::imshow("RGB view", frame_vis);
		//cv::imshow("Selection", frame_selection);
	}

	if (k == 27)
	{
		cout << "ESC pressed!" << endl;
		exit(0);
	}
	
	//	vector<Point> contour_poly;
	//approxPolyDP(Mat(cornerPts), contour_poly, 3, true);
	cv::minEnclosingCircle(cornerPts, center, radius);
	cv::circle(frame_selection, center, (int)radius, Scalar(0, 255, 0), 1, 8, 0);
	cv::line(frame_vis, cornerPts[0], cornerPts[cornerPts.size() - 1], Scalar(0, 255, 0));
	cv::line(frame_selection, cornerPts[0], cornerPts[cornerPts.size() - 1], Scalar(0, 255, 0));

	//cv::imshow("Selection", frame_selection);
	cv::imshow("RGB view", frame_vis);
}


void autoAdjust(Point2f &center, float &radius, int cornerIndex, VideoCapture cap){
	Mat frame, frame_vis, frame_HSV, frame_thresholded;
	bool cornersFound = false;
	bool keyPressed = false;
	int k = -1;
	// scalars for color thresholding
	vector<Scalar> minScalars, maxScalars;
	//initMinMaxScalars(minScalars, maxScalars);
	readMinMaxScalars(minScalars, maxScalars);
	SimpleBlobDetector::Params params = initBlobParas();
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
	lightingParam lp;
	

	vector<KeyPoint> cornersVector;

	while ((k = waitKey(30)) != 27 && (!cornersFound || !keyPressed)){


		if (k == 32) //space key pressed
		{
			keyPressed = true;
		}

		for (int i = 0; i < 8; i++)
		{
			isValidCorner[i] = false;
		}

		cap >> frame;
		frame_vis = frame.clone();
		cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);

		string text = string("Auto-adjusting corner: ") + vertexColors[cornerIndex];
		//drawText(frame_vis, text, Scalar(255));
		drawText(frame_vis, text, Scalar(255, 255, 255));
		
		if (cornerIndex>0)
		{
			lp = lParams[cornerIndex - 1];
			inRange(frame_HSV, Scalar(lp.iLowH, lp.iLowS, lp.iLowV), Scalar(lp.iHighH, lp.iHighS, lp.iHighV), frame_thresholded); //Threshold the image
		}

		vector<thread> threads;

		for (int i = 0; i < NUMCORNERS; i++)
		{
			threads.push_back(thread(detectBlobs, frame_HSV, minScalars, maxScalars, detector, i));
		}
		//segment corners of different colors & put them into different frames 
		// Detect Corners
		for (int i = 0; i < NUMCORNERS; i++)
		{
			if (threads[i].joinable())
			{
				threads[i].join();
			}
		}


		for (int i = 0; i < NUMCORNERS; i++)
		{
			if (isValidCorner[i])
			{
				cornersVector.push_back(corners[i]);
			}

		}

		/*if (cornersVector.size() > NUMCORNERS)	//some corners are missing
		{
			keyPressed = false; // wait until all the corners are found
			string text = "Missing corner(s): ";
			for (int i = 0; i < NUMCORNERS; i++)
			{
				if (isValidCorner[i] == false)
				{
					text +=  vertexColors[i] + " ";
				}
			}
			drawText3(frame_vis, text, Scalar(255, 255, 255));
		}*/
		//else
		//{
			string text2 = "Press SPACE key to confirm";
			drawText2(frame_vis, text2, Scalar(255, 255, 255));
			cornersFound = true;
		//}


		drawKeypoints(frame_vis, cornersVector, frame_vis, Scalar(0, 255, 0), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);

		if (isValidCorner[cornerIndex])
		{
			center = corners[cornerIndex].pt;
			radius = corners[cornerIndex].size / 3;	 //shrink the range for later use ... 2/3 of radius

			circle(frame_vis, center, radius, Scalar(0, 0, 255), 3);
		}
		

		//drawLinesRGB(frame_vis, corners, isValidCorner);
		cornersVector.clear();
		if (cornerIndex > 0)
		{
			GaussianBlur(frame_thresholded, frame_thresholded, Size(9, 9), 0, 0);
			cv::imshow("Thresholded Image", frame_thresholded);
		}
		cv::imshow("RGB view", frame_vis);
	}

	if (k == 27)
	{
		cout << "ESC pressed!" << endl;
		exit(0);
	}
}


lightingParam configLighting(int cornerIndex, VideoCapture cap){
	Mat frame, frame_vis, frame_HSV, frame_thresholded,frame_selection;
	int k = -1;

	//initialize values for the trackbar of color parameters
	lightingParam lp,outLp;
	lp.iLowH = 0;
	lp.iHighH = 179;
	lp.iLowS = 0;
	lp.iHighS = 255;
	lp.iLowV = 0;
	lp.iHighV = 255;

	createHSVControlBar(lp);

	//find the bounding circle for the corner
	Point2f center;
	float radius;

	if (isAutoAdjusting)
	{
		autoAdjust(center, radius, cornerIndex, cap);
	}
	else
	{
		selectCorner(center, radius, cornerIndex, cap);
	}
	

	lightingParam prevLP, currentLP,finalLP,initLP;
	initLP = lp;
	prevLP = initLP;
	currentLP = initLP;
	finalLP = initLP;
	double st = PI*radius*radius;
	double TPR = 0;

	bool isFirst = true;
	//bool isSecond = false;
	double correctionRatio = -1;

	breakWhileFlag = false;
	int count = 0;
	//phase one: find the upper bound
	while ((k = waitKey(30)) != 27 && !breakWhileFlag) // capture frame until ESC is pressed
	{
		cap >> frame;
		frame_vis = frame.clone();  
		cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);
		if (isMannual)
		{
			inRange(frame_HSV, Scalar(lp.iLowH, lp.iLowS, lp.iLowV), Scalar(lp.iHighH, lp.iHighS, lp.iHighV), frame_thresholded); //Threshold the image
		}
		else
		{
			inRange(frame_HSV, Scalar(currentLP.iLowH, currentLP.iLowS, currentLP.iLowV), Scalar(currentLP.iHighH, currentLP.iHighS, currentLP.iHighV), frame_thresholded); //Threshold the image
		}

		

		double s1 = computeArea(cropImage(frame_thresholded, center, radius, false));
		
		if (isFirst)
		{
			TPR = s1 / st;
			correctionRatio = 1 / TPR;
			isFirst = false;
		}

		TPR = s1 * correctionRatio / st;
		cout << "S1: " << s1 << " TPR: "<<TPR<<endl;

		if (!isMannual)
		{
			if (count == 0)
			{
				if (TPR>0.95)
				{
					prevLP = currentLP;
					currentLP.iLowH++;
				}
				else{

					finalLP.iLowH = prevLP.iLowH;
					currentLP = initLP;
					count++;
				}

			}
			else if (count == 1){
				if (TPR > 0.95)
				{

					prevLP = currentLP;
					currentLP.iHighH--;
				}
				else{
					finalLP.iHighH = prevLP.iHighH;

					//initLP = finalLP;
					currentLP = initLP;
					count++;
					isFirst = true;
				}

			}
			else if (count == 2){
				if (TPR >0.95)
				{
					prevLP = currentLP;
					currentLP.iLowS++;
				}
				else
				{
					finalLP.iLowS = prevLP.iLowS;
					currentLP = initLP;
					count++;
				}
			}
			else if (count == 3){
				if (TPR >0.95)
				{
					prevLP = currentLP;
					currentLP.iHighS--;
				}
				else
				{
					finalLP.iHighS = prevLP.iHighS;
					currentLP = finalLP;
					count++;
					breakWhileFlag = true;
				}
			}

			updateLP(currentLP);
		}
		
		circle(frame_vis, center, (int)radius, Scalar(0, 255, 0), 1, 8, 0);
		cv::imshow("Thresholded Image", frame_thresholded);
		cv::imshow("RGB view", frame_vis);
	}

	if (k == 27)
	{
		cout << "ESC pressed!" << endl;
		exit(0);
	}

	//init everything
	prevLP = finalLP;
	currentLP = finalLP;
	initLP = finalLP;

	isFirst = true;
	double initValue = -1;

	breakWhileFlag = false;
	count = 0;

	float maxArea;
	int maxIndex;

	//correct the detected circle by applying blob detector


	//Phase Two: expand the thresholds to make it more accurate/robust
	SimpleBlobDetector::Params params = initBlobParas();
	Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(params);
	vector<KeyPoint> keypoints;
	while ((k = waitKey(30)) != 27 && !breakWhileFlag) // capture frame until ESC is pressed
	{
		keypoints.clear();
		cap >> frame;
		frame_vis = frame.clone();
		cvtColor(frame_vis, frame_HSV, CV_BGR2HSV);
		if (isMannual)
		{
			inRange(frame_HSV, Scalar(lp.iLowH, lp.iLowS, lp.iLowV), Scalar(lp.iHighH, lp.iHighS, lp.iHighV), frame_thresholded); //Threshold the image
		}
		else
		{
			inRange(frame_HSV, Scalar(currentLP.iLowH, currentLP.iLowS, currentLP.iLowV), Scalar(currentLP.iHighH, currentLP.iHighS, currentLP.iHighV), frame_thresholded); //Threshold the image
		}

		//correct the detected circle by applying blob detector
		
		GaussianBlur(frame_thresholded, frame_thresholded, Size(9, 9), 0, 0);
		Mat frame_thresholded_inverted = cv::Scalar::all(255) - frame_thresholded;
		detector->detect(frame_thresholded_inverted, keypoints);
		//find the largest blob
		maxArea = -1;
		maxIndex = -1;
		
		for (int i = 0; i < keypoints.size(); i++)
		{
			if (keypoints[i].size > maxArea){
				maxIndex = i;
				maxArea = keypoints[i].size;
			}
		}
		if (maxIndex >= 0  && ptDist(keypoints[maxIndex].pt,center) <= 5) //the max blob is found
		{
			//update the center and the radius
			center = keypoints[maxIndex].pt;
			radius = keypoints[maxIndex].size / 2;
		}

		double s1 = computeArea(cropImage(frame_thresholded, center, radius, false));
		double s2 = computeArea(cropImage(frame_thresholded, center, radius, true));

		double value = s1 / (s1 + s2);
		if (isFirst)
		{
			initValue = value;
			isFirst = false;
		}

		cout << "s1/(s1+s2) = " << value << endl;

		if (!isMannual)
		{
			if (count == 0)		//low S //need expand
			{
				if (value > 0.98 && currentLP.iLowS > 0)
				{
					prevLP = currentLP;
					currentLP.iLowS--;
				}
				else{

					finalLP.iLowS = (prevLP.iLowS + initLP.iLowS) / 2;
					currentLP = initLP;
					count++;
				}

			}
			else if (count == 1)		//high S
			{
				if (value > 0.98 && currentLP.iHighS < 255)
				{
					prevLP = currentLP;
					currentLP.iHighS++;
				}
				else{

					finalLP.iHighS = (prevLP.iHighS + initLP.iHighS) / 2;
					initLP = finalLP;
					currentLP = initLP;
					count++;
				}

			}
			//else if (initValue > 0.98)			
			//{
				
				else if (count == 2)	//low H
				{
					if (value>0.98 && currentLP.iLowH > 0)
					{
						prevLP = currentLP;
						currentLP.iLowH--;
					}
					else{

						finalLP.iLowH = (prevLP.iLowH + initLP.iLowH) / 2;
						currentLP = initLP;
						count++;
					}

				}
				else if (count == 3 )		//high H
				{
					if (value>0.98 && currentLP.iHighH < 179)
					{
						prevLP = currentLP;
						currentLP.iHighH++;
					}
					else{

						finalLP.iHighH = (prevLP.iHighH + initLP.iHighH) / 2;
						//initLP = finalLP;
						currentLP = finalLP;
						count++;
						breakWhileFlag = true;
					}

				}
				
			//}

			updateLP(currentLP);
		}

		
		if (k == 27)
		{
			cout << "ESC pressed!" << endl;
			exit(0);
		}

		circle(frame_vis, center, (int)radius, Scalar(0, 255, 0), 2, 8, 0);
		circle(frame_thresholded, center, (int)radius, Scalar(0, 255, 0), 3, 8, 0);
		cv::imshow("Thresholded Image", frame_thresholded);
		cv::imshow("RGB view", frame_vis);
	}

	return finalLP;
}

map<int, string> initVertexColors(){
	map<int, string> output;
	output[0] = "green";
	output[1] = "orange";
	output[2] = "red";
	output[3] = "yellow";
	output[4] = "cyan"; 
	output[5] = "pink";
	output[6] = "purple";
	output[7] = "blue";
	return output;

}



/**  Main program  **/
int main(int argc, char *argv[])
{



	// scalars for color thresholding
	vector<Scalar> minScalars, maxScalars;


	/**initialize frames**/
	vector<KeyPoint> cornersVector;

	bool inputFinished = false;
	while (!inputFinished)
	{
		//user-input the adjusting mode 1: Mannual 2: Auto
		cout << "Please choose the type of adujustment:\n 1-Mannual\n 2-Automatic" << endl;
		
		string inputStr;
		getline(cin, inputStr);
		//int admode = stoi(inputStr);
		if (inputStr.compare("1") == 0)
		{
			isAutoAdjusting = false;
			inputFinished = true;
		}
		else if (inputStr.compare("2")==0){
			isAutoAdjusting = true;
			inputFinished = true;
		}
		else
		{
			cout << "Invalid input!" << endl;
		}
	}
	

	nameWindows();

	VideoCapture cap(1);
	if (!cap.isOpened())   // check if we succeeded
	{
		std::cout << "Could not open the camera device" << endl;
		return -1;
	}

	vertexColors = initVertexColors();

	
	for (int i = 0; i < NUMCORNERS; i++)
	{
		cout << "configuring corner: " << i << endl;
		lParams.push_back(configLighting(i,cap));
	}

	lightingParam red;
	red.iLowH = 178;
	red.iLowS = 208;
	red.iLowV = 0;
	red.iHighH = 179;
	red.iHighS = 255;
	red.iHighV = 255;

	lParams.push_back(red);

	writeParams(lParams);
	cap.release();

}

void initSrcCorners(Mat imgToDisplay, vector<Point2f> &srcCorners){
	srcCorners.push_back(Point2f(0, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, 0));
	srcCorners.push_back(Point2f(imgToDisplay.cols, imgToDisplay.rows));
	srcCorners.push_back(Point2f(0, imgToDisplay.rows));
}

void drawLinesRGB(Mat frame, KeyPoint corners[], map<int, bool> isValidCorner){
	// draw lines in the rgb frame to show the cube
	for (int i = 0; i < 4; i++){
		if (isValidCorner[i] && isValidCorner[(i + 1) % 4]){
			line(frame, corners[i].pt, corners[(i + 1) % 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i + 4] && isValidCorner[(i + 1) % 4 + 4])
		{
			line(frame, corners[i + 4].pt, corners[(i + 1) % 4 + 4].pt, Scalar(0, 255, 0), 3);
		}
		if (isValidCorner[i] && isValidCorner[i + 4])
		{
			line(frame, corners[i].pt, corners[i + 4].pt, Scalar(0, 255, 0), 3);
		}
	}

}

void detectCorners(KeyPoint corners[], map<int, bool> &isValidCorner, Mat inputFrame, Ptr<SimpleBlobDetector> detector, vector<KeyPoint> keypoints, int index){
	float maxArea = -1;
	int maxIndex = -1;

	inputFrame = cv::Scalar::all(255) - inputFrame;
	detector->detect(inputFrame, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		isValidCorner[index] = false;
	}

	//drawKeypoints(inputFrame, keypoints, inputFrame, Scalar(0, 0, 255), DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
}

void createBloBControlBar(int &minThreshold, int &maxThreshold, int &minArea, int &minCircularity, int &minConvexity, int &minInertiaRatio){
	cvCreateTrackbar("minThreshold", "Control Blob Detection", &minThreshold, 255); //minThreshold (0 - 255)
	cvCreateTrackbar("maxThreshold", "Control Blob Detection", &maxThreshold, 255); //maxThreshold (0 - 255)
	cvCreateTrackbar("minArea", "Control Blob Detection", &minArea, 3000); //minArea (0 - 307200 (=640*480) )
	cvCreateTrackbar("minCircularity", "Control Blob Detection", &minCircularity, 100); //minCircularity (0-1) --- /100 to be done
	cvCreateTrackbar("minConvexity", "Control Blob Detection", &minConvexity, 100); //minConvexity (0 - 1) --- /100 to be done
	cvCreateTrackbar("minInertiaRatio", "Control Blob Detection", &minInertiaRatio, 100); //minInertiaRatio (0 - 1)

}

void createHSVControlBar(lightingParam &lp){

	//Create trackbars in "Control" window
	cvCreateTrackbar("LowH", "Control", &(lp.iLowH), 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &(lp.iHighH), 179);
	cvCreateTrackbar("LowS", "Control", &(lp.iLowS), 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &(lp.iHighS), 255);
	cvCreateTrackbar("LowV", "Control", &(lp.iLowV), 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &(lp.iHighV), 255);
}

void nameWindows(){

	if (SHOWBLOB)
	{
		namedWindow("Control Blob Detection", CV_WINDOW_AUTOSIZE);
		namedWindow("Color Blob", 1);
	}

	//create windows to show frames
	/*namedWindow("H Channel", 1);
	namedWindow("S Channel", 1);
	namedWindow("V Channel", 1);*/

	if (SHOWTHRESH)
	{
		namedWindow("Thresholded Image", 1);
		namedWindow("Control", CV_WINDOW_AUTOSIZE); //create a window called "Control"
		//namedWindow("Selection", CV_WINDOW_AUTOSIZE);
	}

	/*namedWindow("Green Corner", 1);
	namedWindow("Blue Corner", 1);
	namedWindow("Yellow Corner", 1);
	namedWindow("Purple Corner", 1);
	namedWindow("Red Corner", 1);
	namedWindow("Orange Corner", 1);*/
	namedWindow("RGB view", CV_WINDOW_AUTOSIZE);

	cv::moveWindow("RGB view", 0, 0);
	cv::moveWindow("Thresholded Image", 0, 520);
	cv::moveWindow("Control", 650, 520);
}

SimpleBlobDetector::Params initBlobParas(){
	//note: different from the original version
	SimpleBlobDetector::Params params;

	//initialize values for the trackbar of blob detector
	// Change thresholds
	params.minThreshold = 239;
	params.maxThreshold = 255;

	// Filter by Area.
	params.filterByArea = true;
	params.minArea = 288;

	// Filter by Circularity
	//params.filterByCircularity = true;
	params.minCircularity = 0.01;

	// Filter by Convexity
	//params.filterByConvexity = true;
	params.minConvexity = 0.00;

	// Filter by Inertia
	params.filterByInertia = true;
	params.minInertiaRatio = 0.36;

	return params;
}

/**********************************************************************************************************/
void initKalmanFilter(KalmanFilter &KF, int nStates, int nMeasurements, int nInputs, double dt)
{

	KF.init(nStates, nMeasurements, nInputs, CV_64F);                 // init Kalman Filter

	setIdentity(KF.processNoiseCov, Scalar::all(1e-5));       // set process noise
	setIdentity(KF.measurementNoiseCov, Scalar::all(1e-2));   // set measurement noise
	setIdentity(KF.errorCovPost, Scalar::all(1));             // error covariance


	/** DYNAMIC MODEL **/

	//  [1 0 0 dt  0  0 dt2   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 1 0  0 dt  0   0 dt2   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 1  0  0 dt   0   0 dt2 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  1  0  0  dt   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  1  0   0  dt   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  1   0   0  dt 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   1   0   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   1   0 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   1 0 0 0  0  0  0   0   0   0]
	//  [0 0 0  0  0  0   0   0   0 1 0 0 dt  0  0 dt2   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 1 0  0 dt  0   0 dt2   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 1  0  0 dt   0   0 dt2]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  1  0  0  dt   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  1  0   0  dt   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  1   0   0  dt]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   1   0   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   1   0]
	//  [0 0 0  0  0  0   0   0   0 0 0 0  0  0  0   0   0   1]

	// position
	KF.transitionMatrix.at<double>(0, 3) = dt;
	KF.transitionMatrix.at<double>(1, 4) = dt;
	KF.transitionMatrix.at<double>(2, 5) = dt;
	KF.transitionMatrix.at<double>(3, 6) = dt;
	KF.transitionMatrix.at<double>(4, 7) = dt;
	KF.transitionMatrix.at<double>(5, 8) = dt;
	KF.transitionMatrix.at<double>(0, 6) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(1, 7) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(2, 8) = 0.5*pow(dt, 2);

	// orientation
	KF.transitionMatrix.at<double>(9, 12) = dt;
	KF.transitionMatrix.at<double>(10, 13) = dt;
	KF.transitionMatrix.at<double>(11, 14) = dt;
	KF.transitionMatrix.at<double>(12, 15) = dt;
	KF.transitionMatrix.at<double>(13, 16) = dt;
	KF.transitionMatrix.at<double>(14, 17) = dt;
	KF.transitionMatrix.at<double>(9, 15) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(10, 16) = 0.5*pow(dt, 2);
	KF.transitionMatrix.at<double>(11, 17) = 0.5*pow(dt, 2);


	/** MEASUREMENT MODEL **/

	//  [1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0]
	//  [0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0]

	KF.measurementMatrix.at<double>(0, 0) = 1;  // x
	KF.measurementMatrix.at<double>(1, 1) = 1;  // y
	KF.measurementMatrix.at<double>(2, 2) = 1;  // z
	KF.measurementMatrix.at<double>(3, 9) = 1;  // roll
	KF.measurementMatrix.at<double>(4, 10) = 1; // pitch
	KF.measurementMatrix.at<double>(5, 11) = 1; // yaw

}

/**********************************************************************************************************/
void updateKalmanFilter(KalmanFilter &KF, Mat &measurement,
	Mat &translation_estimated, Mat &rotation_estimated)
{

	// First predict, to update the internal statePre variable
	Mat prediction = KF.predict();

	// The "correct" phase that is going to use the predicted value and our measurement
	Mat estimated = KF.correct(measurement);

	// Estimated translation
	translation_estimated.at<double>(0) = estimated.at<double>(0);
	translation_estimated.at<double>(1) = estimated.at<double>(1);
	translation_estimated.at<double>(2) = estimated.at<double>(2);

	// Estimated euler angles
	Mat eulers_estimated(3, 1, CV_64F);
	eulers_estimated.at<double>(0) = estimated.at<double>(9);
	eulers_estimated.at<double>(1) = estimated.at<double>(10);
	eulers_estimated.at<double>(2) = estimated.at<double>(11);

	// Convert estimated quaternion to rotation matrix
	rotation_estimated = euler2rot(eulers_estimated);

}

/**********************************************************************************************************/
void fillMeasurements(Mat &measurements,
	const Mat &translation_measured, const Mat &rotation_measured)
{
	// Convert rotation matrix to euler angles
	Mat measured_eulers(3, 1, CV_64F);
	measured_eulers = rot2euler(rotation_measured);

	// Set measurement to predict
	measurements.at<double>(0) = translation_measured.at<double>(0); // x
	measurements.at<double>(1) = translation_measured.at<double>(1); // y
	measurements.at<double>(2) = translation_measured.at<double>(2); // z
	measurements.at<double>(3) = measured_eulers.at<double>(0);      // roll
	measurements.at<double>(4) = measured_eulers.at<double>(1);      // pitch
	measurements.at<double>(5) = measured_eulers.at<double>(2);      // yaw
}



void initiPointModel(vector<Point3f> &list_points3d_model){
	list_points3d_model.push_back(Point3f(-5.0f, -5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, -5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, -5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, -5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, 5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, 5.0f, 5.0f));
	list_points3d_model.push_back(Point3f(5.0f, 5.0f, -5.0f));
	list_points3d_model.push_back(Point3f(-5.0f, 5.0f, -5.0f));
}


void detectBlobs(Mat frame_HSV, vector<Scalar> mins, vector<Scalar> maxs, Ptr<SimpleBlobDetector> detector, int index){
	Mat frame_thresh;
	vector<KeyPoint> keypoints;

	if (index != 2)
	{
		//if not red
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner
	}
	else if (index == 2)
	{
		Mat frame_red2;
		inRange(frame_HSV, mins[index], maxs[index], frame_thresh); //Threshold the  corner
		
		inRange(frame_HSV, mins[8], maxs[8], frame_red2); //Threshold the red corner ---- the second part
		cv::add(frame_thresh, frame_red2, frame_thresh);
		

	}


	float maxArea = -1;
	int maxIndex = -1;
	//blur(frame_thresh, frame_thresh, Size(9, 9), Point(-1, -1));
	GaussianBlur(frame_thresh, frame_thresh, Size(9, 9), 0, 0);
	mtx.lock();
	if (SHOWBLOB)
	{
		cv::add(frameBlob, frame_thresh, frameBlob);
	}
	mtx.unlock();

	frame_thresh = cv::Scalar::all(255) - frame_thresh;


	detector->detect(frame_thresh, keypoints);


	//find the largest blob
	for (int i = 0; i < keypoints.size(); i++)
	{
		if (keypoints[i].size > maxArea){
			maxIndex = i;
			maxArea = keypoints[i].size;
		}
	}

	if (maxIndex >= 0) //the max blob is found
	{
		mtx.lock();
		corners[index] = keypoints[maxIndex];
		isValidCorner[index] = true;
		mtx.unlock();

	}
	else
	{
		//	cout << "Corner " << index << " is missing" << endl;
		mtx.lock();
		isValidCorner[index] = false;
		mtx.unlock();
	}
	//cout << "Thread     " << index << "finished!" << endl;
}

void readMinMaxScalars(vector<Scalar> &minScalars, vector<Scalar> &maxScalars){

	ifstream iFile("lightingParams.txt");
	string line;
	if (!iFile.is_open())
	{
		cout << "Can't open lighting parameters! " << endl;
	}

	while (iFile)
	{
		if (!getline(iFile, line))
		{
			break;
		}
		stringstream ss(line);
		vector <int> record;

		int count = 0;
		while (ss){
			string s;
			if (!getline(ss, s, ','))	break;
			record.push_back(stoi(s));
		}
		minScalars.push_back(Scalar(record[0], record[1], record[2]));
		maxScalars.push_back(Scalar(record[3], record[4], record[5]));
	}
	iFile.close();

}

void writeParams(vector<lightingParam> lParams){
	string oFileName = "lightingParams.txt";
	ofstream oFile;
	oFile.open(oFileName);
	for (int i = 0; i < NUMCORNERS + 1; i++)		//because red has two parameters
	{
		string thisline = to_string(lParams[i].iLowH) + ","
			+ to_string(lParams[i].iLowS) + ","
			+ to_string(lParams[i].iLowV) + ","
			+ to_string(lParams[i].iHighH) + ","
			+ to_string(lParams[i].iHighS) + ","
			+ to_string(lParams[i].iHighV) + "\n";
		oFile << thisline;
	}
	oFile.close();
}