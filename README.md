This repository includes the source code of the Automatic HSV Range Adjustment System of the CAPTIVE project. You may want to run this project before CAPTIVE to configure the lighting parameters according to your environment.

CAPTIVE is a 6DoF input device to manipulate 3D virtual objects.

# Environment Configuration

    1. Install OpenCV and make sure it works on your machine. Check this link for more details:
    http://docs.opencv.org/2.4/doc/tutorials/introduction/windows_install/windows_install.html

    2. Install Visual Studio 2013 or later version.

# How to setup
    
    1. Download source code.
    2. Create a new blank c++ Win32 Console Application in VS2013.
    3. Add all the source code to that project.
    4. Add the property sheet (OpencvSheet.props) to that project.
    5. Build and run.